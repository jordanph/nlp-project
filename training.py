import os
import torch
import argparse
import random
import pandas as pd
from transformers import AutoTokenizer, AutoModelForSequenceClassification, AdamW

import utils

class SequenceClassificationModel(torch.nn.Module):
    def __init__(self, model_huggingface):
        super(SequenceClassificationModel, self).__init__()
        self.model_huggingface = model_huggingface

    def forward(self, **inputs):
        outputs = self.model_huggingface(**inputs)
        return outputs["logits"]

def main(args):
    if args.verbose: print("reading data...")
    df = pd.read_csv(args.path_data, sep=",")
    df = df.sample(frac=1) # shuffle
    texts = df["text"].to_list()
    labels = df["label"].to_list()
    split = int(args.split * len(labels))
    texts_train = texts[:split]
    texts_test = texts[split:]
    labels_train = labels[:split]
    labels_test = labels[split:]
    if args.verbose:
        print("%d training samples" % split)
        print("%d test samples" % (len(labels) - split))

    # create output directory
    if not os.path.isdir(args.dir_output):
        os.makedirs(args.dir_output)

    if args.verbose: print("loading tokenizers...")
    tokenizer = AutoTokenizer.from_pretrained(args.checkpoint_tokenizer)
    collate_fn = utils.TextCollator(tokenizer)

    ds_train = utils.TextDataset(texts_train, labels_train) 
    ds_test = utils.TextDataset(texts_test, labels_test) 
    dl_train = torch.utils.data.DataLoader(
        dataset=ds_train,
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=4,
        collate_fn=collate_fn
    )
    dl_test = torch.utils.data.DataLoader(
        dataset=ds_test,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=4,
        collate_fn=collate_fn
    )

    if args.verbose: print("loading models...")
    model_huggingface = AutoModelForSequenceClassification.from_pretrained(args.checkpoint_model, num_labels=2)
    model = SequenceClassificationModel(model_huggingface)

    fn_loss = torch.nn.CrossEntropyLoss()

    train(model, dl_train, dl_test, fn_loss, args)

def move_to_device(x, device):
    if torch.is_tensor(x):
        x = x.to(device)
    elif isinstance(x, dict):
        for key in x:
            x[key] = move_to_device(x[key], device)
    else:
        for idx in range(len(x)):
            x[idx] = move_to_device(x[idx], device)
    return x

def evaluation(model, dataloader, device):
    """ estimate accuracy of model

    Args:
        model : model to evaluate
        dataloader : data to evaluate on
        device : torch device

    Returns:
        accuracy of model on given data
    """
    model.eval()
    count_correct = 0.0
    with torch.no_grad():
        for i, batch in enumerate(dataloader):
            inputs = batch["inputs"]
            labels = batch["labels"]
            inputs = move_to_device(inputs, device)
            labels = move_to_device(labels, device)
            logits = model(**inputs)
            preds = torch.argmax(logits, dim=1)
            count_correct += torch.sum(preds == labels).item()
    accuracy = count_correct / len(dataloader.dataset)
    return accuracy

def train(model, dl_train, dl_test, fn_loss, args):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)

    optimizer = AdamW(model.parameters(), lr=5e-5)
    scaler = torch.cuda.amp.GradScaler(enabled=args.mixed_precision)

    if args.verbose: print("training...")
    for epoch in range(args.epochs):
        model.train()
        avg_loss = 0.0
        for i, batch in enumerate(dl_train):
            inputs = batch["inputs"]
            labels = batch["labels"]
            inputs = move_to_device(inputs, device)
            labels = move_to_device(labels, device)
            with torch.cuda.amp.autocast(enabled=args.mixed_precision):
                preds = model(**inputs)
                loss = fn_loss(preds, labels)
                loss /= args.accumulation_size
            scaler.scale(loss).backward()
            avg_loss += loss.item()
            if (i + 1) % args.accumulation_size == 0:
                scaler.step(optimizer)
                scaler.update()
                optimizer.zero_grad() 
                if args.verbose:
                    print(
                        "epoch %d/%d, batch %d/%d, avg. loss: %.3f" %
                        (epoch+1, args.epochs, (i+1)//args.accumulation_size, len(dl_train)//args.accumulation_size, avg_loss)
                    )
                avg_loss = 0.0
        # evaluation
        accuracy = evaluation(model, dl_test, device)
        if args.verbose: print("evaluation...")
        print("accuracy: %.5f" % accuracy)
        # save model parameters to specified file
        checkpoint = {
            "epoch": epoch + 1,
            "model_state_dict": model.state_dict(),
            "optimizer_state_dict": optimizer.state_dict(),
            "scaler_state_dict": scaler.state_dict(),
            "accuracy": accuracy,
        }
        path_checkpoint = os.path.join(args.dir_output, "checkpoint_%d" % (epoch+1))
        torch.save(checkpoint, path_checkpoint)
   
if __name__ == "__main__":
    os.environ["TRANSFORMERS_CACHE"] = os.path.join(os.environ["SCRATCH"], ".cache")

    parser = argparse.ArgumentParser(description='train ensemble model on data')
    
    # command line arguments
    # io
    parser.add_argument('path_data', type=str, 
        help='path to data', action='store')
    parser.add_argument('dir_output', type=str, 
        help='directory where model checkpoints should be stored', action='store')
    parser.add_argument('-ckptt', '--checkpoint_tokenizer', type=str,
        help='path to pretrained tokenizer that should be used')
    parser.add_argument('-ckptm', '--checkpoint_model', type=str,
        help='path to pretrained model that should be used')
    parser.add_argument('-v', '--verbose', 
        help='set to have verbose output', action='store_true')

    # training
    parser.add_argument('-e', '-epochs', dest='epochs', type=int, 
        help='number of epochs to train', action='store', default=3)
    parser.add_argument('-bs', '--batch_size', dest='batch_size', type=int, 
        help='size of batches for training', action='store', default=8)
    parser.add_argument('-as', '--accumulation_size', dest='accumulation_size', type=int, 
        help='reduces memory usage, if larger', action='store', default=4)
    parser.add_argument('--seed', dest='seed', type=int, 
        help='fix random seeds', action='store', default=42)
    parser.add_argument('--split', dest='split', type=float, 
        help='define train/test split, number between 0 and 1', action='store', default=0.8)
    parser.add_argument('-mp', '--mixed_precision', dest='mixed_precision',
        help='set to enable mixed precision training', action='store_true', default=False)

    args = parser.parse_args()

    # set seeds
    torch.manual_seed(args.seed)
    random.seed(args.seed)

    # start training
    main(args)

