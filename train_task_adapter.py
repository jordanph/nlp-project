import os
import torch
import argparse
import random
from transformers import BertTokenizer, BertForSequenceClassification
from transformers import AdamW
from transformers import AdapterConfig
from transformers.adapters.composition import Stack
import pandas as pd

import utils

def evaluation(args, model, dataloader, device):
    # evaluate i.e. generate accuracy estimation
    if args.verbose: print("evaluation...")
    model.eval()
    count_correct = 0.0
    with torch.no_grad():
        for i, batch in enumerate(dataloader):
            inputs = batch["inputs"]
            labels = batch["labels"]
            inputs = utils.move_to_device(inputs, device)
            labels = utils.move_to_device(labels, device)
            outputs = model(**inputs, labels=labels)
            preds = torch.argmax(outputs[1], dim=1)
            count_correct += torch.sum(preds == labels).item()
    accuracy = count_correct / len(dataloader.dataset)
    print("accuracy: %.5f" % accuracy)

def main(args):
    # get the data
    if args.verbose: print("reading data...")
    df = pd.read_csv(args.path_data, sep=",")
    df = df.sample(frac=1) # shuffle
    texts = df["text"].to_list()
    labels = df["label"].to_list()
    split = int(args.split * len(labels))
    texts_train = texts[:split]
    texts_test = texts[split:]
    labels_train = labels[:split]
    labels_test = labels[split:]
    if args.verbose:
        print("%d training samples" % split)
        print("%d test samples" % (len(labels) - split))

    # create output directory
    if not os.path.isdir(args.dir_adapter_task):
        os.makedirs(args.dir_adapter_task)

    # get the tokenizer
    if args.verbose: print("loading tokenizer...")
    tokenizer = BertTokenizer.from_pretrained(args.dir_tokenizer) 
    collate_fn = utils.TextCollator(tokenizer)

    # build dataloader
    ds_train = utils.TextDataset(texts_train, labels_train) 
    ds_test = utils.TextDataset(texts_test, labels_test) 
    dl_train = torch.utils.data.DataLoader(
        dataset=ds_train,
        batch_size=args.batch_size//args.accumulation_size,
        shuffle=True,
        num_workers=4,
        collate_fn=collate_fn
    )
    dl_test = torch.utils.data.DataLoader(
        dataset=ds_test,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=4,
        collate_fn=collate_fn
    )

    # use gpu if possible
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # load pretrained model
    model = BertForSequenceClassification.from_pretrained(args.dir_model)
    name_adapter_task = "sentiment_twitter"
    model.add_adapter(name_adapter_task)
    if args.dir_adapter_lang == None:
        model.active_adapters = name_adapter_task
    else:
        config_adapter_lang = AdapterConfig.load("pfeiffer+inv", non_linearity="gelu", reduction_factor=2)
        name_adapter_lang = model.load_adapter(args.dir_adapter_lang, config=config_adapter_lang)
        model.active_adapters = Stack(name_adapter_lang, name_adapter_task)
    model.train_adapter([name_adapter_task])
    model.to(device)
    
    optimizer = AdamW(model.parameters(), lr=5e-5)

    # train
    if args.verbose: print("training...")
    for epoch in range(args.epochs):
        model.train()
        avg_loss = 0.0
        for i, batch in enumerate(dl_train):
            inputs = batch["inputs"]
            labels = batch["labels"]
            inputs = utils.move_to_device(inputs, device)
            labels = utils.move_to_device(labels, device)
            outputs = model(**inputs, labels=labels)
            loss = outputs["loss"]
            loss /= args.accumulation_size
            loss.backward()
            avg_loss += loss.item()
            if (i + 1) % args.accumulation_size == 0:
                optimizer.step()
                optimizer.zero_grad() 
                if args.verbose:
                    print(
                        "epoch %d/%d, batch %d/%d, avg. loss: %.3f" %
                        (epoch+1, args.epochs, i//args.accumulation_size, len(dl_train)//args.accumulation_size, avg_loss)
                    )
                avg_loss = 0.0
        evaluation(args, model, dl_test, device)
        # save model parameters to specified file
        model.save_adapter(os.path.join(args.dir_adapter_task, "checkpoint_%d" % (epoch + 1)), name_adapter_task)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='train pretrained BERT model on data')

    parser.add_argument('path_data', type=str, 
        help='path to training data')
    parser.add_argument('-v', '--verbose', dest='verbose', 
        help='want verbose output or not?', action='store_true')
    parser.add_argument('-e', '-epochs', dest='epochs', type=int, 
        help='number of epochs to train', action='store', default=3)
    parser.add_argument('-bs', '--batch_size', dest='batch_size', type=int, 
        help='size of batches for training', action='store', default=16)
    parser.add_argument('-as', '--accumulation_size', dest='accumulation_size', type=int, 
        help='reduces memory usage, if larger', action='store', default=16)
    parser.add_argument('--seed', dest='seed', type=int, 
        help='fix random seeds', action='store', default=42)
    parser.add_argument('--split', dest='split', type=float, 
        help='define train/test split, number between 0 and 1', action='store', default=0.8)
    parser.add_argument(
        "--dir_tokenizer",
        dest="dir_tokenizer",
        help="directory containing files of pretrained tokenizer"
    )
    parser.add_argument(
        "--dir_model",
        dest="dir_model",
        help="directory containing files of pretrained model"
    )
    parser.add_argument(
        "--dir_adapter_task",
        dest="dir_adapter_task",
        help="directory containing files of pretrained task adapter"
    )
    parser.add_argument(
        "--dir_adapter_lang",
        dest="dir_adapter_lang",
        help="directory containing files of pretrained language adapter"
    )

    args = parser.parse_args()

    # set seeds
    torch.manual_seed(args.seed)
    random.seed(args.seed)

    main(args)

