import torch

def move_to_device(x, device):
    if torch.is_tensor(x):
        x = x.to(device)
    elif isinstance(x, dict):
        for key in x:
            x[key] = move_to_device(x[key], device)
    else:
        for idx in range(len(x)):
            x[idx] = move_to_device(x[idx], device)
    return x

class TextDataset(torch.utils.data.Dataset):
    def __init__(self, texts, labels):
        self.texts = texts
        self.labels = labels

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        return self.texts[idx], self.labels[idx]

class TextCollator():
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer

    def __call__(self, list_items):
        texts = [item[0] for item in list_items]
        encodings = self.tokenizer(texts, truncation=True, padding=True)
        batch = {}
        batch["inputs"] = {key: torch.tensor(val) for key, val in encodings.items()}
        labels = [item[1] for item in list_items]
        batch["labels"] = torch.tensor(labels)
        return batch

