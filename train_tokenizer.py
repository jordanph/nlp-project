import os
import argparse
from tokenizers import BertWordPieceTokenizer

def main(args):
    tokenizer = BertWordPieceTokenizer(
        vocab=None,
        clean_text=True,
        handle_chinese_chars=False,
        strip_accents=None,
        lowercase=True,
    )
    tokenizer.train(
        files=[args.path_data],
        vocab_size=30000,
        min_frequency=2,
        special_tokens=["[PAD]", "[UNK]", "[CLS]", "[SEP]", "[MASK]"],
        show_progress=True,
    )
    if not os.path.exists(args.dir_output):
        os.makedirs(args.dir_output)
    tokenizer.save_model(args.dir_output)
    tokenizer.save(os.path.join(args.dir_output, "tokenizer_config.json"))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='train tokenizer on data')

    parser.add_argument('path_data', type=str, 
        help='path to training data')
    parser.add_argument('dir_output', type=str, 
        help='directory to write outputs to')

    args = parser.parse_args()

    main(args)
