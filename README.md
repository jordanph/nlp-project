# Transfer Learning For Low-Resource Languages

This is the repository correspondig to our work [Transfer Learning For Low-Resource Languages](reportTransferLearningForLowResourceLanguages.pdf).

## Instructions

To reproduce our results follow these instructions.

### Environment

Start by creating a virtual environment,
```bash
python3 -m venv .venv
```

then activate the virtual environment
```bash
source .venv/bin/activate
```

and load the necessary requirements
```bash
pip install -r requirements.txt
```

### Datasets

We have set up a shared [polybox folder](https://polybox.ethz.ch/index.php/s/rxnAtl8JVmLYjQS) where you can find all our datasets.
Download the needed files and move them to a convenient location.
Subsequently we assume all data files are at $SCRATCH/data.

### Training

To train the plain mBERT model on a binary classification task use the training.py script.
For example to finetune mBERT on the filmstarts dataset run
```bash
python3 training.py -ckptt bert-base-multilingual-cased -ckptm bert-base-multilingual-cased -bs 8 -as 4 --seed 69 -v $SCRATCH/data/filmstarts.csv $SCRATCH/checkpoints/de_noadapter
```

To train a task adapter use the train_task_adapter.py script.
For example to get our sentiment analysis task adapter trained on the imdb dataset run
```bash
python3 train_task_adapter.py -v -e 3 -bs 8 -as 4 --dir_tokenizer bert-base-multilingual-cased --dir_model bert-base-multilingual-cased --dir_adapter_lang en/wiki@ukp --dir_adapter_task $SCRATCH/checkpoints/adapter_task_imdb $SCRATCH/data/imdb.csv
```
and use the checkpoint of epoch 3.

To train a tokenizer on your data use the train_tokenizer.py script.
For example to get our Swiss German tokenizer run
```bash
python3 train_tokenizer.py $SCRATCH/data/plaintext_ch.txt $SCRATCH/checkpoints/tokenizer_ch
```

To train a new language adapter use the run_mlm.py script.
For example to get our Swiss German language adapter starting with the pretrained German language
adapter of AdapterHub as initialization and using the normal mBERT tokenizer run
```bash
python3 run_mlm.py --model_name_or_path bert-base-multilingual-cased --train_file $SCRATCH/data/plaintext_ch.txt --per_device_train_batch_size 4 --gradient_accumulation_steps 8 --max_steps 100000 --save_strategy steps --save_steps 10000 --do_train --train_adapter --adapter_config pfeiffer+inv --adapter_non_linearity gelu --load_adapter de/wiki@ukp --output_dir $SCRATCH/checkpoints/adapter_ch_fromde --seed 1234
```
and use the 10000 step checkpoint.

### Evaluation

To evaluate use the evaluation.py script.
For example to evaluate the model with Swiss German language adapter trained from the
German adapter initialization and imdb task adapter on the sbch dataset run
```bash
python3 evaluation.py -v --dir_tokenizer bert-base-multilingual-cased --dir_model bert-base-multilingual-cased --dir_adapter_task $SCRATCH/checkpoints/adapter_task_imdb/checkpoint_3 --dir_adapter_lang $SCRATCH/checkpoints/adapter_ch_fromde/checkpoint-10000/mlm --size_batch 64 $SCRATCH/data/sbch.csv
```

### Leonhard

If you work on leonhard follow these instructions.

First you need to load some required modules:
```bash
module load gcc/6.3.0 python_gpu/3.8.5 eth_proxy
```

After that we recommend you run all commands with the following resource requests:
```bash
bsub -R "rusage[mem=8192, ngpus_excl_p=1]" -R "select[gpu_model0==GeForceGTX1080Ti]" command
```

