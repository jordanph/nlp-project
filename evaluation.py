from argparse import ArgumentParser
import torch
from transformers import BertTokenizer, BertForSequenceClassification
from transformers import AdapterConfig
from transformers.adapters.composition import Stack
import os
import pandas as pd
import numpy as np

import utils

def get_accuracy(logits, labels, cutoff=0.5):
    tmp = np.apply_along_axis(lambda row: 1 if row[0] < cutoff else 0, axis=1, arr=logits)
    tmp = (tmp == labels)
    return tmp.sum() / len(labels)

def main(args):
    device = "cuda" if torch.cuda.is_available() else "cpu"
    if args.verbose: print("using %s" % device)

    if args.verbose: print("reading data...")
    df = pd.read_csv(args.path_data, sep=",")
    texts = df["text"].to_list()
    labels = df["label"].to_list()
    if args.verbose:
        print("%d test samples" % len(texts))

    if args.verbose: print("loading tokenizer...")
    tokenizer = BertTokenizer.from_pretrained(args.dir_tokenizer)
    collate_fn = utils.TextCollator(tokenizer)

    if args.verbose: print("preparing dataloader...")
    ds = utils.TextDataset(texts, labels)
    dl = torch.utils.data.DataLoader(
        dataset=ds,
        batch_size=args.size_batch,
        shuffle=False,
        num_workers=4,
        collate_fn=collate_fn
    )

    if args.verbose: print("loading model...")
    model = BertForSequenceClassification.from_pretrained(args.dir_model)
    #model = BertForSequenceClassification.from_pretrained("bert-base-multilingual-cased")
    #checkpoint = torch.load(args.dir_model, map_location=device)
    #model_state_dict = {key[18:]: val for key, val in checkpoint["model_state_dict"].items()}
    #model.load_state_dict(model_state_dict)
    list_names_adapters = []
    if args.dir_adapter_lang != None:
        name_adapter_task = model.load_adapter(args.dir_adapter_task, with_head=True)
        list_names_adapters.append(name_adapter_task)
    if args.dir_adapter_lang != None:
        config_adapter_lang = AdapterConfig.load("pfeiffer+inv", non_linearity="gelu", reduction_factor=2)
        name_adapter_lang = model.load_adapter(args.dir_adapter_lang, config=config_adapter_lang, with_head=False)
        list_names_adapters.append(name_adapter_lang)
    model.active_adapters = Stack(*list_names_adapters)
    model.to(device)
    
    if args.verbose: print("evaluating...")
    model.eval()
    count_correct = 0.0
    logits_all = np.zeros(shape=(0,2))
    labels_all = np.array(labels)
    with torch.no_grad():
        for batch in dl:
            inputs = batch["inputs"]
            labels = batch["labels"]
            inputs = utils.move_to_device(inputs, device)
            labels = utils.move_to_device(labels, device)
            outputs = model(**inputs, labels=labels)
            logits = outputs["logits"]
            logits_all = np.concatenate([logits_all, logits.cpu().numpy()], axis=0)
            preds = torch.argmax(logits, dim=1)
            count_correct += torch.sum(preds == labels).item()
            for i in range(labels.shape[0]):
                print("%d %.5f %.5f" % (labels[i], logits[i][0], logits[i][1])) 
    accuracy = count_correct / len(ds)
    print("accuracy: %.5f" % accuracy)
    logits_all = torch.nn.Softmax(dim=1)(torch.tensor(logits_all))
    cutoffs = np.linspace(0, 1, 101)
    accs = np.vectorize(lambda cutoff: get_accuracy(logits_all, labels_all, cutoff))(cutoffs)
    print("corrected accuracy: %.5f (cutoff: %.2f)" % (np.max(accs), np.argmax(accs)*0.01))

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        dest='verbose', 
        help='want verbose output or not?'
    )
    parser.add_argument(
        "--dir_tokenizer",
        dest="dir_tokenizer",
        default="Pretrained/tokenizer_ch",
        help="directory containing files of pretrained tokenizer"
    )
    parser.add_argument(
        "--dir_model",
        dest="dir_model",
        default="Pretrained/model_sequence_classification",
        help="directory containing files of pretrained model"
    )
    parser.add_argument(
        "--dir_adapter_task",
        dest="dir_adapter_task",
        default="Pretrained/adapter_sentiment",
        help="directory containing files of pretrained task adapter"
    )
    parser.add_argument(
        "--dir_adapter_lang",
        dest="dir_adapter_lang",
        default=None,
        help="directory containing files of pretrained language adapter"
    )
    parser.add_argument(
        "--size_batch",
        type=int,
        dest="size_batch",
        default=32,
        help="batch size"
    )
    parser.add_argument(
        "path_data",
        help="path of data file"
    )
    args = parser.parse_args()
    main(args)
